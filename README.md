WIP repo to work on "Graph consensus and vote information from Rob's experiment (May/June 2021)" (https://gitlab.torproject.org/tpo/network-health/analysis/-/issues/5
)

### Experiment:

> to investigate the accuracy of the advertised bandwidth a
relay is reporting in its server descriptor.[1] Back then this involved,
for each relay, downloading several large data streams for a period of
about 20 seconds to get a better estimation of the relay's true capacity

### Dates experiment:

[2021-05-17 16:00:00 UTC, Around 2021-06-05 00:00:00 UTC]

### Needed graphs:

- [ ] CDF-TTFB: (latency) time-to-first-byte of a 5MB download (from onionperf)
- [ ] CDF-DL: (throughput) the average bandwidth of the second half of a 5Mb download (from onionperf?)
- [ ] CDF-Relay-Utilization:
    average read/write history over time for a relay divided by peak advertised bandwidth over that period of time, for each relay in the network.
    X axis range of [0.0, 1.0].
    This should produce a value between 0 and 1.
    It can also be broken down per-flag (so that there are separate CDFs generated for Guard, Middle, Exit, and Guard+Exit flagged relays).
    ```
    read_write := (read + written) / 2;
    ~~advertised := min(rate, burst, observed)~~
    ~~peak := max(rate, burst, observed)~~
    peak: = observed
    ~~plot -> advertised / read_write~~
    plot -> read_write / peak
    ```
  - [ ] CDF-Relay-Stream-Capacity:
    `relay_balance_ratio = relay_measured_bw / relay_observed_bw`.
    This balance ratio is equivalent to `relay_stream_bw / network_avg_stream_bw`.
    using either the consensus or the individual votes, compute relay_balance_ratio = relay_measured_bw / relay_observed_bw. This balance ratio is equivalent to relay_stream_bw / network_avg_stream_bw
    can be broken down by relay flags, as well (Guard, Middle, Exit, Guard+Exit).
    For Torflow:
    ```
    plot -> 1000*measured / min(rate, observed)
    ```
    For sbws:
    ```
    plot -> 1000*measured / min(rate, observed)
    ```

Description of the graphs to analyse the data: https://gitlab.torproject.org/legacy/trac/-/wikis/org/roadmaps/CoreTor/PerformanceMetrics

### To observe:

...

### My questions

1. Are onionperf instances already enough to obtain the CDF-TTFB and CDF-DL graphs?, do they need some "manual" processing?
1. Any of the graph we need already at https://metrics.torproject.org/torperf.html?
1. How many days to take before or after the experiment?
   It looks like Karsten took ~5 days and dennis a moving avg [was it for this?, need to check]
1. Do the Torflow to sbws transition affect some more graphs or observations than the CDF-Relay-Stream-Capacity ones?
1. Do all the current graph tooling (except bwauthealth that process collector documents into a DBMS) take input data as CSV?
1. It looks like `utilization.py`  in the sims repo has graphs for Tor live data, are there already any graphs?

### Torflow to sbws transition

They affect to CDF-Relay-Stream-Capacity graphs.
There're already graphs at https://gitlab.torproject.org/tpo/network-health/analysis/-/issues/6

## Software tools that have been or are being used

Since the experiment will be repeated and analyzed more times, it'd be nice to have prepared tooling for it and do not duplicate work.

### onionperf

(https://gitlab.torproject.org/tpo/network-health/metrics/onionperf)

Looks like the base method to create the graphs is https://gitlab.torproject.org/tpo/network-health/metrics/onionperf/-/blob/develop/onionperf/visualization.py#L486

Uses pandas and matplotlib

### tornettools
(https://github.com/shadow/tornettools)

Files to create the graphs:
- https://github.com/shadow/tornettools/blob/main/tornettools/plot_common.py
- https://github.com/shadow/tornettools/blob/main/tornettools/plot.py

Uses matplotlib

### sponsor-61-sims

(https://gitlab.torproject.org/jnewsome/sponsor-61-sims/)

Hiro seems to have been working on https://gitlab.torproject.org/jnewsome/sponsor-61-sims/-/blob/98aeede4ffbafd1d05eab7fe6c3b1b06a30b5eaf/utilization.py

Main function to create the graphs?: https://gitlab.torproject.org/jnewsome/sponsor-61-sims/-/blob/98aeede4ffbafd1d05eab7fe6c3b1b06a30b5eaf/utilization.py#L127

### Dennis' Jupyter Notebook

https://drive.google.com/open?id=1q1JRP5RdPEhQcDddh7KEST1aADMoiyt_ (copied locally)

### Karsten's tools

a mix of java and R?, reusing some metrics code?

### bwauthealth

(https://gitlab.torproject.org/juga/bwauthealth)

Used for Torflow to sbws CDF-Relay-Stream-Capacity graphs.

Uses plotly, pandas and a DBMS.

## Work done to analyse the experiment in 2019

(https://gitlab.torproject.org/tpo/network-health/metrics/analysis/-/issues/33076)

All the graphs are copied locally, some were wrong [to specify].

`op-ab`,  `op-nl` and `op-us` were onionperf instances.

Main report?: https://web.archive.org/web/20200617231215/https://people.torproject.org/~karsten/volatile/onionperf-metrics-2019-02-02.pdf

CDF-TTFB:
- https://gitlab.torproject.org/tpo/network-health/metrics/analysis/uploads/6d29fd269ada9c436bfc2b76fc04d89d/onionperf-cdf-ttfb-2020-01-28.pdf
- https://gitlab.torproject.org/tpo/network-health/metrics/analysis/uploads/833b9ebf18e0f87773da038ef84bb339/onionperf-cdf-ttfb-2020-01-28a.pdf
- https://gitlab.torproject.org/tpo/network-health/metrics/analysis/uploads/5e43ebb216090454cf8d82a5d2e8b0cf/onionperf-cdf-ttfb-2020-02-11.pdf
- https://dennisjj.co.uk/tor-bandwidth-experiment/ttfb_public_op-ab.png
- https://dennisjj.co.uk/tor-bandwidth-experiment/raw/exit_op-ab.png

CDF_DL:
- https://gitlab.torproject.org/legacy/trac/uploads/f401f43fe2a42332adc75d3d1c1f74a8/onionperf.viz.2020-05-28_20_49_42.pdf (automatically generated by onionperf?)
-
CDF-Relay-Utilization and CDF-Relay-Stream-Capacity:
- https://gitlab.torproject.org/tpo/network-health/metrics/analysis/uploads/f10b7035838cf39a1cf30cd3abdb61f8/cdf-relay-utilization-2020-01-29.pdf
- https://gitlab.torproject.org/tpo/network-health/metrics/analysis/uploads/afecb1cf8f0815604bd4c200f424bded/cdf-relay-utilization-and-stream-capacity-2020-01-29a.pdf
- https://gitlab.torproject.org/tpo/network-health/metrics/analysis/uploads/45f1e14fff0b0602e23541127063f288/cdf-relay-utilization-and-stream-capacity-2020-02-04.pdf
